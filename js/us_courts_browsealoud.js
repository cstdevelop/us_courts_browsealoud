/**
 * @file
 * A JavaScript file for CST themes and browsealoud module.
 *
 */
 (function ($) {

   Drupal.behaviors.usc_courts_browsealoud = {

     attach: function(context,settings) {


        browseAloud = '<li class="browsealoud"><div id="__ba_panel" class="_ba_manual">';
        browseAloud += '<div id="_ba__button_link"><a id="_ba__link" href="javascript:;" ';
        browseAloud += 'onclick="BrowseAloud.panel.toggleBar(true,event);return false;"><i class="fa fa-assistive-listening-systems fa-1x fa-fw"></i><span>Listen to Page</span></a></div></div></li>';

        if (window.matchMedia('(max-width: 960px)').matches)  {
             $mobileNav = $('nav.mean-nav ul');
             $mobileNav.append(browseAloud);
        }

        $(window).resize(function(){
           if ($(window).width() <= 960){
             $mobileNav = $('nav.mean-nav ul');
             $mobileNav.append(browseAloud);
           }
       });


     }
   };

 }(jQuery));
